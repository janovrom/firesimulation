# FireSimulation

A simple system simulating the spreading of fire among plants while taking into account the direction and speed of the wind.
More information on [wiki](https://gitlab.com/janovrom/firesimulation/wikis/Project-Info) and build available [here](https://gitlab.com/janovrom/firesimulation/blob/master/FireSimulation/Build/FireSimulation-janovsky.zip)