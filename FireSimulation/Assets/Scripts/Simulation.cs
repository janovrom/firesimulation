﻿using ECS.Objects;
using System.Collections.Generic;
using UnityEngine;

public class Simulation : MonoBehaviour
{

    [Header("Plant Generation")]
    public PoissonPlantGenerator Generator;
    public Terrain Terrain;
    public Transform PlantContainer;

    [Header("Simulation Settings")]
    public BoxCollider SimulationRegion;
    public float MaximumFireSpread = 15f;
    public LayerMask FlammableMask;
    public Vector3 WindDirection;
    [Range(0f, 103f)] // 103m/s is the fastest measured speed at the Mt. Washington
    public float WindSpeedMetersPerSec = 0f;
    public GameObject WindGizmo;
    [HideInInspector]
    public string TerrainTag;

    [Header("Internal stuff")]
    [HideInInspector]
    [SerializeField]
    private Plant[] _plants;
    [SerializeField]
    private BoolBinding _simulationPlaying;

    public int PlantCount => _plants == null ? -1 : _plants.Length;
    private Vector3 _defaultHeatPropagation = new Vector3(0, 1, 0);
    private int _lastNonBurning = -1;
    private int _lastBurning = -1;
    private bool _initializedPlants = false;

    private Collider[] _colliderBuffer = new Collider[256];


    private void Awake()
    {
        WindGizmo.transform.rotation = Quaternion.identity;
    }

    void Update()
    {
        if (!_simulationPlaying.Value)
            return;

        if (Input.GetMouseButtonDown(0) && Input.GetKey(KeyCode.LeftShift))
        {
            CreatePlant();
        }
        else if (Input.GetMouseButtonDown(0) && Input.GetKey(KeyCode.LeftControl))
        {
            RemovePlant();
        }
        else if (Input.GetMouseButtonDown(0) && Input.GetKey(KeyCode.LeftAlt))
        {
            ToggleFire();
        }

        if (_initializedPlants)
            SpreadFire();
    }

    private void LateUpdate()
    {
        WindGizmo.transform.position = Camera.main.transform.position + Camera.main.transform.forward * 2f + Camera.main.transform.right * 0.5f;
    }

    private void ToggleFire()
    {
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit hit, 1000f, FlammableMask))
        {
            var p = hit.collider.GetComponent<Plant>();
            if (!ReferenceEquals(p, null))
            {
                int idx = p.SimulationIndex;
                if (_lastNonBurning < 0 || idx <= _lastNonBurning) // Ignore the burned down
                {
                    if (p.IsFireSource)
                    {
                        p.Reset();
                        MoveAsFirstNonBurning(idx);
                    }
                    else
                    {
                        StartFire(idx);
                    }

                    UpdateIndices();
                }
            }
        }
    }

    private void RemovePlant()
    {
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit hit, 1000f, FlammableMask))
        {
            var p = hit.collider.GetComponent<Plant>();
            if (!ReferenceEquals(p, null))
            {
                p.RemoveItself();
                int idx = p.SimulationIndex;
                var plantCopy = new Plant[_plants.Length - 1];
                System.Array.Copy(_plants, 0, plantCopy, 0, idx);
                System.Array.Copy(_plants, idx + 1, plantCopy, idx, _plants.Length - idx - 1);
                _plants = plantCopy;

                if (idx <= _lastBurning) // Fire source
                {
                    --_lastBurning;
                    --_lastNonBurning;
                }
                else if (idx <= _lastNonBurning) // non burning plant
                {
                    --_lastNonBurning;
                }
                Destroy(p.gameObject);

                UpdateIndices();
            }
        }
    }

    private void CreatePlant()
    {
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit hit, 1000f))
        {
            if (hit.collider.gameObject.CompareTag(TerrainTag))
            {
                var p = Generator.Create(hit.point);
                if (_plants == null || _plants.Length == 0)
                {
                    _plants = new Plant[] { p };
                    ++_lastNonBurning;
                }
                else
                {
                    var plantCopy = new Plant[_plants.Length + 1];
                    System.Array.Copy(_plants, 0, plantCopy, 0, ++_lastNonBurning);
                    plantCopy[_lastNonBurning] = p;
                    System.Array.Copy(_plants, _lastNonBurning, plantCopy, _lastNonBurning + 1, _plants.Length - _lastNonBurning);
                    _plants = plantCopy;

                    //_notBurning.Add(p);
                }

                if (_initializedPlants)
                {
                    p.Initialize(MaximumFireSpread, FlammableMask, _colliderBuffer);
                    p.InsertItselfToClosePlants();
                }

                p.transform.SetParent(PlantContainer, true);

                UpdateIndices();
            }
        }
    }

    private void UpdateIndices()
    {
        for (int i = 0; i < _plants.Length; ++i)
            _plants[i].SimulationIndex = i;
    }

    private void SpreadFire()
    {
        float deltaTime = Time.deltaTime;
        var heatDir = _defaultHeatPropagation + WindDirection.normalized * WindSpeedMetersPerSec;
        int i = 0;
        // Update fire sources
        for (; i < _lastBurning + 1; ++i)
        {
            _plants[i].UpdateLocalSurroundings(deltaTime, ref heatDir);
            if (_plants[i].UpdateBurningTime(deltaTime))
            {
                PlaceBurnedLast(i);
            }
        }

        // Update potential sources
        for (; i < _lastNonBurning + 1; ++i)
        {
            if (_plants[i].Temperature > _plants[i].Data.FlashPointTemperature)
            {
                if (_plants[i].CanFireStart(deltaTime))
                {
                    _plants[i].LightOnFire();
                    ConcatToFireSources(i);
                }
            }
        }

        // Update burned down plants
        float inverseDeltaTime = 1f - deltaTime;
        for (; i < _plants.Length; ++i)
        {
            // When burned down, lower slowly the temperature to 0
            _plants[i].Temperature = Mathf.Max(_plants[i].Temperature * inverseDeltaTime, 0f);
        }

        // Smooth the temperatures except for the fire source
        for (i = _lastBurning + 1; i < _plants.Length; ++i)
        {
            _plants[i].SmoothTemperature(deltaTime);
        }
    }

    private void PlaceBurnedLast(int idx)
    {
        var dead = _plants[idx];
        var lastFireSource = _plants[_lastBurning];
        _plants[_lastBurning--] = _plants[_lastNonBurning];
        _plants[_lastNonBurning--] = dead;
        _plants[idx] = lastFireSource;
    }

    private void MoveAsFirstNonBurning(int idx)
    {
        var fireSource = _plants[_lastBurning];
        _plants[_lastBurning--] = _plants[idx];
        _plants[idx] = fireSource;
    }

    private void ConcatToFireSources(int idx)
    {
        var nonBurning = _plants[++_lastBurning];
        _plants[_lastBurning] = _plants[idx];
        _plants[idx] = nonBurning; 
    }

    private void OnValidate()
    {
        Debug.Assert(Generator != null);
        Debug.Assert(WindGizmo != null);
        Debug.Assert(_simulationPlaying != null);
        Debug.Assert(PlantContainer != null);
    }

    public void Reset()
    {
        _lastBurning = -1;
        _lastNonBurning = - 1;
        _initializedPlants = false;
        _simulationPlaying.Value = false;

        if (_plants == null)
            return;

        foreach (var plant in _plants)
        {
            plant.Reset();
        }
    }

    public void PlacePlants()
    {
        ClearPlants();
        var cx = transform.position.x;
        var cy = transform.position.z;
        Rect rect = new Rect(cx, cy, SimulationRegion.size.x, SimulationRegion.size.z);
        _plants = Generator?.Generate(rect, gameObject.transform.rotation);
        foreach (var plant in _plants)
            plant.transform.SetParent(PlantContainer, true);

        _lastNonBurning = _plants.Length - 1;
        UpdateIndices();
    }

    public void ClearPlants()
    {
        Reset();
        Generator?.Clear(_plants);
        _plants = null;
    }

    public void ToggleSimulation()
    {
        _simulationPlaying.Value = !_simulationPlaying.Value;
    }

    private void StartFire(int idx)
    {
        if (!_initializedPlants)
        {
            for (int i = 0; i < _plants.Length; ++i)
                _plants[i].Initialize(MaximumFireSpread, FlammableMask, _colliderBuffer);
            _initializedPlants = true;
            _lastNonBurning = _plants.Length - 1;
        }

        _plants[idx].LightOnFire();
        ConcatToFireSources(idx);
    }

    public void StartFireOnPlant()
    {
        if (_plants == null || _plants.Length == 0 || !_simulationPlaying.Value) return;

        int idx = Random.Range(0, _plants.Length);
        StartFire(idx);
        FocusCameraOnFirstFireSource();
    }

    public void FocusCameraOnFirstFireSource()
    {
        if (_lastBurning < 0)
            return;

        var source = _plants[_lastBurning];
        if (source.IsFireSource)
        {
            var cam = Camera.main;
            var moveDir = cam.transform.position - source.transform.position;
            cam.transform.position = 20f * moveDir.normalized + source.transform.position;
            if (Physics.Raycast(new Ray(cam.transform.position + Vector3.up * 10000f, Vector3.down), out RaycastHit hit))
            {
                cam.transform.position = hit.point + Vector3.up * 20f;
            }
            cam.transform.LookAt(source.transform);
        }
    }

    public void SetWindSpeed(float speed)
    {
        WindSpeedMetersPerSec = Mathf.Max(Mathf.Min(speed, 103f), 0f);
    }

    public void SetWindDirection(float angleDeg)
    {
        var angleRad = Mathf.Deg2Rad * angleDeg;
        WindDirection.x = Mathf.Cos(angleRad);
        WindDirection.z = -Mathf.Sin(angleRad);

        WindGizmo.transform.rotation = Quaternion.Euler(0f, angleDeg, 0f);
    }

}
