﻿using UnityEngine;
using Versus.Utility.Variables;

public class FPSCounter : MonoBehaviour
{
    public IntVariable HighestFPS;
    public IntVariable LowestFPS;
    public IntVariable AverageFPS;

    private int[] _fpsBuffer;
    private int _fpsBufferIndex;
    private int _frameRange = 60;


    private void Update()
    {
        if (_fpsBuffer == null || _fpsBuffer.Length != _frameRange)
        {
            InitializeBuffer();
        }

        UpdateBuffer();
        CalculateFPS();
    }

    private void UpdateBuffer()
    {
        _fpsBuffer[_fpsBufferIndex++] = (int)(1f / Time.unscaledDeltaTime);
        if (_fpsBufferIndex >= _frameRange)
        {
            _fpsBufferIndex = 0;
        }
    }

    private void CalculateFPS()
    {
        int sum = 0;
        int highest = 0;
        int lowest = int.MaxValue;
        for (int i = 0; i < _frameRange; i++)
        {
            int fps = _fpsBuffer[i];
            sum += fps;
            if (fps > highest)
            {
                highest = fps;
            }
            if (fps < lowest)
            {
                lowest = fps;
            }
        }
        AverageFPS.variable = sum / _frameRange;
        HighestFPS.variable = highest;
        LowestFPS.variable = lowest;
    }

    private void InitializeBuffer()
    {
        if (_frameRange <= 0)
        {
            _frameRange = 1;
        }
        _fpsBuffer = new int[_frameRange];
        _fpsBufferIndex = 0;
    }
}
