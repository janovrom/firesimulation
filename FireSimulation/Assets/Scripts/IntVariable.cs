﻿using UnityEngine;

namespace Versus.Utility.Variables
{
    [CreateAssetMenu(fileName ="New Int", menuName ="Variables/Int")]
    public class IntVariable : Variable<int>
    {
    }
}