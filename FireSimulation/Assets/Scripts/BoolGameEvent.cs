﻿using System.Collections.Generic;
using UnityEngine;


namespace ECS.Events
{

    [CreateAssetMenu(fileName ="NewGameEvent", menuName ="Create Bool Game Event")]
    public class BoolGameEvent : ScriptableObject
    {

        [SerializeField]
        private List<BoolGameEventListener> listeners = new List<BoolGameEventListener>();


        public void AddListener(BoolGameEventListener listener)
        {
            listeners.Add(listener);
        }

        public void RemoveListener(BoolGameEventListener listener)
        {
            listeners.Remove(listener);
        }

        public void Invoke(bool arg)
        {
            for (int i = listeners.Count - 1; i >= 0; --i)
            {
                listeners[i].OnEventRaised(arg);
            }
        }
    }

}