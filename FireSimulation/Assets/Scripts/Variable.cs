﻿using UnityEngine;


namespace Versus.Utility.Variables
{

    public abstract class Variable<T> : ScriptableObject, ISerializationCallbackReceiver
    {

        [SerializeField]
        private T defaultVariable;
        public T variable;


        private void OnEnable()
        {
            variable = defaultVariable;    
        }

        public void OnAfterDeserialize()
        {
            // Called during instantiation for example
            variable = defaultVariable;
        }

        public void OnBeforeSerialize()
        {
        }
    }

}