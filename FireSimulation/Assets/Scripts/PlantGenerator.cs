﻿using UnityEngine;

public abstract class Generator<T> : ScriptableObject
{

    public abstract void Clear(T[] plants);
    public abstract T Create(Vector3 pos);
    public abstract T[] Generate(Rect bounds, Quaternion boundsRotation);

}
