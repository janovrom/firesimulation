﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PoissonPlantGenerator", menuName = "Create Poisson Plant Generator")]
public class PoissonPlantGenerator : Generator<Plant>
{

    public float MinimumRadius = 5f;
    public int MaximumTries = 30;
    public FlammableData[] FlammablePrototypes;
    public Plant PlantPrefab;


    private void OnValidate()
    {
        Debug.Assert(FlammablePrototypes != null && FlammablePrototypes.Length > 0);
        Debug.Assert(PlantPrefab != null);
    }

    private bool IsValidPoint(Vector2 p, float rSqr, int x, int y, ref int[,] grid, ref List<Vector2> points)
    {
        if (x < 0 || x >= grid.GetLength(0) || y < 0 || y >= grid.GetLength(1))
            return false;

        int x0 = Mathf.Max(x - 1, 0);
        int x1 = Mathf.Min(x + 1, grid.GetLength(0) - 1);
        int y0 = Mathf.Max(y - 1, 0);
        int y1 = Mathf.Min(y + 1, grid.GetLength(1) - 1);

        for (int i = x0; i <= x1; ++i)
        {
            for (int j = y0; j <= y1; ++j)
            {
                if (grid[i, j] > -1) // Not empty
                {
                    if ((points[grid[i, j]] - p).sqrMagnitude < rSqr) // Not far enough
                        return false;
                }
            }
        }

        return true;
    }

    public override void Clear(Plant[] plants)
    {
        if (plants == null)
            return;

        for (int i = plants.Length - 1; i >= 0; --i)
        {
            if (plants[i] != null)
#if UNITY_EDITOR
                DestroyImmediate(plants[i].gameObject);
#else
            Destroy(plants[i].gameObject);
#endif
        }

        plants = null;
    }

    public override Plant[] Generate(Rect bounds, Quaternion boundsRotation)
    {
        float startX = bounds.x;
        float startY = bounds.y;
        bounds.x = 0f;
        bounds.y = 0f;

        float r = MinimumRadius;
        float rSq = r * r;
        int k = MaximumTries;

        var cellSize = (r / Mathf.Sqrt(2));
        int W = Mathf.CeilToInt(bounds.size.x / cellSize);
        int H = Mathf.CeilToInt(bounds.size.y / cellSize);
        int[,] grid = new int[W, H];
        for (int i = 0; i < W; ++i)
            for (int j = 0; j < H; ++j)
                grid[i, j] = -1;

        List<int> activeList = new List<int>();
        List<Vector2> positions = new List<Vector2>(W * H);

        // First random sample
        var initPos = bounds.center + Random.insideUnitCircle * cellSize * 0.25f;
        grid[Mathf.FloorToInt(initPos.x / cellSize), Mathf.FloorToInt(initPos.y / cellSize)] = 0;
        activeList.Add(0);
        positions.Add(initPos);

        float[] angles = new float[8];
        float range = 2 * Mathf.PI * 0.125f;
        for (int i = 0; i < 8; ++i)
            angles[i] = 2 * Mathf.PI * (0.125f * i);

        while (activeList.Count > 0)
        {
            // Pick random sample
            int idx = Random.Range(0, activeList.Count);
            var xi = positions[idx];

            bool found = false;
            for (int i = 0; i < k; ++i)
            {
                // Generate k samples in the annulus <r,2r> around xi
                // It's likely that in range <r,2r> the random sample is not found, bigger range gives higher probability for smaller r
                float randomR = Random.Range(r + 0.001f, 2 * bounds.size.magnitude * 0.01f * rSq * rSq); 
                float angle = Random.Range(-range, range) + angles[i % angles.Length];
                Vector2 sample = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));

                // Check range
                var pos = xi + sample * randomR;
                int x = Mathf.FloorToInt(pos.x / cellSize);
                int y = Mathf.FloorToInt(pos.y / cellSize);
                // All around is far enough
                if (bounds.Contains(pos) && IsValidPoint(pos, rSq, x, y, ref grid, ref positions))
                {
                    int newIdx = positions.Count;
                    grid[x, y] = newIdx;
                    positions.Add(pos);
                    activeList.Add(newIdx);
                    found = true;
                    break;
                }
            }

            if (!found)
            {
                activeList[idx] = activeList[activeList.Count - 1];
                activeList.RemoveAt(activeList.Count - 1);
            }

        }

        var plants = new List<Plant>(positions.Count);
        for (int i = 0; i < positions.Count; ++i)
        {
            RaycastHit hit;
            var center = new Vector3(bounds.center.x, 0f, bounds.center.y);
            var pos = new Vector3(positions[i].x, 0f, positions[i].y);
            pos = boundsRotation * (pos - center);
            pos.x += startX;
            pos.y = 10000f;
            pos.z += startY;

            if (Physics.Raycast(new Ray(pos, Vector3.down), out hit))
            {
                // Select random prototype and initialize runtime values
                plants.Add(Create(hit.point));
            }
            else
            {
                Debug.LogWarning("Missed raycast which shouldn't happen.");
            }
        }

        return plants.ToArray();
    }

    public override Plant Create(Vector3 pos)
    {
        var prototypeData = FlammablePrototypes[Random.Range(0, FlammablePrototypes.Length)];
        var plant = Instantiate(PlantPrefab);
        plant.Data = prototypeData;

        plant.transform.position = pos;
        plant.transform.localScale = new Vector3(1f, plant.Height, 1f);

        return plant;
    }

}
