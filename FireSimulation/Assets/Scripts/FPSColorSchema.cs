﻿using UnityEngine;

[CreateAssetMenu(fileName ="NewFpsColorSchema", menuName ="Create Fps Color Schema")]
public class FPSColorSchema : ScriptableObject
{
    [SerializeField]
    public FPSColor[] Colors;
}

[System.Serializable]
public struct FPSColor
{
    public Color Color;
    public int MinimumFPS;
}