﻿namespace ECS.Events
{

    public class NegateBoolGameEventListener : BoolGameEventListener
    {

        public override void OnEventRaised(bool arg)
        {
            actionTaken.Invoke(!arg);
        }

    }

}
