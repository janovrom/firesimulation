﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class Plant : MonoBehaviour
{

    // Required data
    public FlammableData Data;
    public Material DefaultPlantMaterial;
    public Material BurningPlantMaterial;
    public Material BurnedPlantMaterial;

    // Flammable state runtime variables
    public int SimulationIndex = 0;
    public float Height => Data.MassKg * 0.25f;
    public float Temperature = 0f;
    public bool IsFireSource = false;
    private float _timeOverFlashPointTemperature = 0f;
    [SerializeField]
    private float _remainingMassKg;

    // Cached data for speed-up
    private Renderer PlantRenderer;
    private Plant[] _closePlants;
    private float[] _inverseDistances;
    private Vector3[] _directions;
    private Vector3 _position; // Works only for static objects


    private void Awake()
    {
        PlantRenderer = GetComponentInChildren<Renderer>();
        Reset();
    }

    public void Reset()
    {
        if (PlantRenderer != null)
            PlantRenderer.sharedMaterial = DefaultPlantMaterial;

        _position = transform.position;
        Temperature = 0f;
        IsFireSource = false;
        _timeOverFlashPointTemperature = 0f;
        _remainingMassKg = Data.MassKg;
    }

    public void Initialize(float maximumFireSpread, LayerMask flammableMask, Collider[] colliderBuffer)
    {
        Reset();
        int found = Physics.OverlapSphereNonAlloc(_position, maximumFireSpread, colliderBuffer, flammableMask);
        var plants = new List<Plant>(found);
        for (int i = 0; i < found; ++i)
        {
            var collider = colliderBuffer[i];
            var plant = collider.GetComponent<Plant>();
            if (ReferenceEquals(plant, null) || ReferenceEquals(plant, this)) // Speed up, "==" is slow as it compares values
                continue;

            plants.Add(plant);
        }

        _closePlants = plants.ToArray();

        // Initialize arrays for faster computation
        _inverseDistances = new float[_closePlants.Length];
        _directions = new Vector3[_closePlants.Length];
        for (int i = 0; i < _closePlants.Length; ++i)
        {
            var direction = (_closePlants[i]._position - _position);
            _inverseDistances[i] = 1f / direction.magnitude;
            _directions[i] = direction.normalized;
        }
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void SmoothTemperature(float deltaTime)
    {
        if (_closePlants.Length < 1)
            return;

        // Compute average temperature in the local space
        // Update the temperature according to this one
        float accumTemperature = 0f;
        for (int i = 0; i < _closePlants.Length; ++i)
        {
            accumTemperature += _closePlants[i].Temperature * _inverseDistances[i];
        }

        var smoothLocalTemperature = accumTemperature / _closePlants.Length;
        Temperature = Mathf.Lerp(Temperature, smoothLocalTemperature, deltaTime *0.1f);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public bool CanFireStart(float deltaTime)
    {
        _timeOverFlashPointTemperature += deltaTime;
        if (Data.TimeBeforeCatchingFire < _timeOverFlashPointTemperature)
            return true;

        return false;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public bool UpdateBurningTime(float deltaTime)
    {
        _remainingMassKg -= Data.BurnedMassKgPerSec * deltaTime;
        if (_remainingMassKg < 0f)
        {
            IsFireSource = false;
            PlantRenderer.sharedMaterial = BurnedPlantMaterial;
            return true;
        }

        return false;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void UpdateLocalSurroundings(float deltaTime, ref Vector3 heatPropagationDirection)
    {
        var generatedHeat = Data.HeatingValueJoulePerKg * Data.BurnedMassKgPerSec * deltaTime; 
        for (int i = 0; i < _closePlants.Length; ++i)
        {
            var plant = _closePlants[i];
            if (plant.Temperature < plant.Data.FireTemperature && plant.Temperature < Temperature)
            {
                // Apply inverse distance to simulate currents and heat propagation
                var windCoefficient = Vector3.Dot(_directions[i], heatPropagationDirection);
                if (windCoefficient < 0f) // Different direction, make the change smaller
                    plant.Temperature += generatedHeat / (plant.Data.HeatCapacityJoulePerKg * plant._remainingMassKg) * _inverseDistances[i] / (1.0f - windCoefficient);
                else // The same direction, make the change larger
                    plant.Temperature += generatedHeat / (plant.Data.HeatCapacityJoulePerKg * plant._remainingMassKg) * _inverseDistances[i] * (1f + windCoefficient);

                plant.Temperature = Mathf.Min(plant.Temperature, plant.Data.FireTemperature);
            }
        }
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void LightOnFire()
    {
        Temperature = Data.FireTemperature;
        IsFireSource = true;
        _timeOverFlashPointTemperature = Data.TimeBeforeCatchingFire + 0.1f;
        PlantRenderer.sharedMaterial = BurningPlantMaterial;
    }

    public void InsertItselfToClosePlants()
    {
        if (_closePlants == null)
            return;

        for (int i = 0; i < _closePlants.Length; ++i)
            _closePlants[i].InsertPlant(this);
    }

    public void RemoveItself()
    {
        if (_closePlants == null)
            return;

        for (int i = 0; i < _closePlants.Length; ++i)
            _closePlants[i].RemovePlant(this);
    }

    private void RemovePlant(Plant plant)
    {
        int idx = 0;
        int count = _closePlants.Length;
        // Find the index
        for (int i = 0; i < count; ++i)
        {
            if (ReferenceEquals(_closePlants[i], plant))
            {
                idx = i;
                break;
            }
        }

        // Switch with last
        _closePlants[idx] = _closePlants[count - 1];
        _directions[idx] = _directions[count - 1];
        _inverseDistances[idx] = _inverseDistances[count - 1];

        System.Array.Resize(ref _closePlants, count - 1);
        System.Array.Resize(ref _directions, count - 1);
        System.Array.Resize(ref _inverseDistances, count - 1);
    }

    private void InsertPlant(Plant plant)
    {
        int count = _closePlants.Length;
        var closePlants = new Plant[count + 1];
        var directions = new Vector3[count + 1];
        var inverseDistances = new float[count + 1];

        System.Array.Copy(_closePlants, 0, closePlants, 0, count);
        System.Array.Copy(_directions, 0, directions, 0, count);
        System.Array.Copy(_inverseDistances, 0, inverseDistances, 0, count);

        var dir = plant._position - _position;
        closePlants[count] = plant;
        directions[count] = dir.normalized;
        inverseDistances[count] = 1f / dir.magnitude;

        _closePlants = closePlants;
        _directions = directions;
        _inverseDistances = inverseDistances;
    }

    private void OnDrawGizmosSelected()
    {
        if (_closePlants == null)
            return;

        var pos = _position + Vector3.up * Height;
        for (int i = 0; i < _closePlants.Length; ++i)
        {
            Gizmos.DrawLine(pos, _closePlants[i]._position);
        }
    }

}
