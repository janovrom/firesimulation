﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class OpacitySetter : MonoBehaviour
{

    private Image _image;


    private void Awake()
    {
        _image = GetComponent<Image>();
    }

    public void ToggleButton(bool value)
    {
        _image.color = value ? new Color(0.2f, 1f, 0.2f, 1.0f) : new Color(0.2f, 0.2f, 0.2f, 0.5f);
    }

}
