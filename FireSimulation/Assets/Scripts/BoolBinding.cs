﻿using ECS.Events;
using System;
using UnityEngine;


namespace ECS.Objects
{

    [CreateAssetMenu(fileName ="NewBoolBinding", menuName ="Create Bool Binding")]
    [Serializable]
    public class BoolBinding : ScriptableObject
    {

        [SerializeField]
        private bool _defaultValue;

        [SerializeField]
        private bool _runtimeValue;

        public bool Value
        {
            get
            {
                return _runtimeValue;
            }

            set
            {
                if (_runtimeValue != value)
                {
                    _runtimeValue = value;
                    ValueChangedEvent.Invoke(_runtimeValue);
                }
            }
        }

        public BoolGameEvent ValueChangedEvent;


        private void OnEnable()
        {
            _runtimeValue = _defaultValue;
        }

    }
}
