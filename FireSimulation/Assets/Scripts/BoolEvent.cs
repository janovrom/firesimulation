﻿using UnityEngine.Events;

namespace ECS.Events
{

    [System.Serializable]
    public class BoolEvent : UnityEvent<bool>
    {
    }

}