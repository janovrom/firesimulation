﻿using UnityEngine;

[CreateAssetMenu(fileName ="New Flammable Data", menuName ="Create Flammable Data")]
public class FlammableData : ScriptableObject
{

    public float HeatingValueJoulePerKg = 13.4e6f;
    public float HeatCapacityJoulePerKg = 1.76e3f;
    public float FlashPointTemperature = 250f;
    public float FireTemperature = 2000f;
    public float BurnedMassKgPerSec = 20f / 3600f;
    public float TimeBeforeCatchingFire = 16f;
    public float MassKg = 10f;

}
