﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Simulation))]
[CanEditMultipleObjects]
public class SimulationEditor : Editor
{

    private static readonly string _placePlants = "Place plants";
    private static readonly string _clearPlants = "Clear plants";
    private static readonly string _startFire = "Start fire on plant";
    private static readonly string _reset = "Reset";
    private static readonly string _terrainTag = "Terrain tag";
    private static readonly string _plantCount = "Plant count in simulation: ";


    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        var sim = (Simulation)target;
        sim.TerrainTag = EditorGUILayout.TagField(_terrainTag, sim.TerrainTag);
        GUILayout.Label(_plantCount+ sim.PlantCount.ToString());

        if (GUILayout.Button(_placePlants))
        {
            sim.PlacePlants();
        }

        if (GUILayout.Button(_clearPlants))
        {
            sim.ClearPlants();
        }

        if (GUILayout.Button(_startFire))
        {
            sim.StartFireOnPlant();
        }

        if (GUILayout.Button(_reset))
        {
            sim.Reset();
        }
    }

}
